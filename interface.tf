variable "name" {
  description = "The name of vpc"

}
variable "cidr" {
  description = "The CIDR of the vpc"
}

variable "public_subnet" {
  description = "The public subnet to create"
}
variable "enable_dns_hostnames" {
  default = true
}

variable "enable_dns_support" {
  description = "to be true if private DNS within the vpc"
  default = true
}

output "public_subnet_id" {
  value = "${aws_subnet.public.id}"
}
output "vpc_id" {
  value = "${aws_vpc.tfb.id}"
}

output "cidr" {
  value = "${aws_vpc.tfb.cidr_block}"
}
